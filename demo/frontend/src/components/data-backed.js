import CustomStore from "devextreme/data/custom_store";
import notify from "devextreme/ui/notify";
import API from "../services";

export default {
  methods: {
    /*
     * Override the datasource functionality using:
     * - beforeQueryData(...)
     * - convertData(...)
     * - handleDataError(...)
     */
    createDataSource(findFunc, options) {
      // eslint-disable-next-line
      // console.debug("DataBacked::createDataSource", options);
      options =
        options ||
        {
          /*
            insertFunc
            updateFunc
            removeFunc
            mapSort
          */
        };
      const mapSort = options.mapSort || ((selector) => selector);
      let customFilter = () => null;
      if (options.filter) {
        if (typeof options.filter === "function") {
          customFilter = options.filter;
        } else {
          customFilter = () => options.filter;
        }
      }
      const mapSortAndDirection = (selector, desc) => {
        const s = mapSort(selector);
        if (Array.isArray(s)) {
          return s.map((s) => `${s}${desc ? ",DESC" : ""}`);
        } else {
          return [`${s}${desc ? ",DESC" : ""}`];
        }
      };
      const mapRec = options.mapRec || this.convertData || ((rec) => rec);
      const onData = options.onData || (() => true);

      return new CustomStore({
        key: "id",
        load: async (options) => {
          // eslint-disable-next-line
          // console.debug("DataBacked::load", options);

          const query = {
            skip: options.skip,
            take: options.take,
          };

          if (options.sort) {
            let sort = options.sort
              .map((sort) => mapSortAndDirection(sort.selector, sort.desc))
              .reduce((total, val) => {
                val.forEach((val) => total.push(val));
                return total;
              }, []);
            if (sort.length > 0) {
              query.sort = sort;
            }
          }

          let filter = options.filter || null;
          const filterExt = customFilter();
          if (filterExt) {
            if (filter) {
              filter = [filter, "and", filterExt];
            } else {
              filter = filterExt;
            }
          }
          if (filter) {
            query["filter-json"] = JSON.stringify(filter);
          }

          if (
            this.beforeQueryData &&
            typeof this.beforeQueryData === "function"
          ) {
            this.beforeQueryData(query, options);
          }

          try {
            let data = await findFunc(query);
            data.content.forEach((rec) => mapRec(rec));
            data = {
              data: data.content,
              totalCount: data.totalElements,
            };
            // eslint-disable-next-line
            // console.debug("base::list", data);
            onData(data);
            return data;
          } catch (err) {
            if (
              this.handleDataError &&
              typeof this.handleDataError === "function"
            )
              this.handleDataError(err);
            else this.processDataError(err);
          }
        },
        insert: async (data) => {
          // eslint-disable-next-line
          // console.debug("DataBacked::insert", data);
          if (options.insertFunc) return options.insertFunc(data);
        },
        update: async (key, data) => {
          // eslint-disable-next-line
          // console.debug("DataBacked::update", data);
          if (options.updateFunc) return options.updateFunc(key, data);
        },
        remove: async (key) => {
          // eslint-disable-next-line
          // console.debug("DataBacked::remove", data);
          if (options.removeFunc) return options.removeFunc(key);
        },
      });
    },
    processDataError(err) {
      // eslint-disable-next-line
      console.error(err);

      if (err.code === API.ERRORS.AUTH) {
        this.$store.commit("setToken", "");
        if (this.$route && this.$route.go) {
          this.$route.go();
        } else {
          document.location.reload();
        }
      } else if (typeof err.message === "string") {
        notify(err.message.substring(0, 1000), "error", 3000);
      } else {
        // eslint-disable-next-line
        console.error("Error no message", Object.keys(err));
      }
    },
    processRowClick(routeName, e) {
      // eslint-disable-next-line
      // console.debug("DataBacked::processRowClick", e);
      if (
        window.getSelection().focusOffset !== window.getSelection().anchorOffset
      )
        return;

      if (e.event.metaKey || e.event.ctrlKey) {
        const routeData = this.$router.resolve({
          name: routeName,
          params: { id: e.data.id },
        });
        window.open(routeData.href, "_blank");
      } else {
        this.$router.push({
          name: routeName,
          params: { id: e.data.id },
        });
      }
    },
  },
};
