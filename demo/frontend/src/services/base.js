import axios from "axios";
import qs from "qs";
import store from "../store";

const apiUrl = process.env.VUE_APP_API_URL;

const ERRORS = ["GENERIC", "AUTH", "API"]
  .reduce((ERRORS,key)=>{ ERRORS[key] = key; return ERRORS }, {});
export { ERRORS };

export class ErrorWrapper extends Error {
  constructor(code, message, options) {
    super();
    this.name = "ErrorWrapper";
    this.code = code;
    this.message = message;
    if (options) this.options = options;
  }
}

export function trimUrl(url) {
  if (url.indexOf("http") !== 0)
    url = `${window.document.location.origin}${url}`;
  const x = url.indexOf("/rest");
  return x >= 0 ? url.substr(0, x + 5) : url;
}

function makeAxiosError(err) {
  if (err.response && [401, 403].includes(err.response.status)) {
    return new ErrorWrapper(
      ERRORS.AUTH,
      "Invalid username or password",
      { httpStatus: err.httpStatus }
    );
  } else if (err.response && err.response.status === 404) {
    return new ErrorWrapper(ERRORS.API, `Server connection failed: ${trimUrl(err.config.url)}`,
      { httpStatus: err.response.status }
    );
  } else if (err.response && err.response.data && err.response.data.message) {
    return new ErrorWrapper(ERRORS.API, err.response.data.message, {
      originalError: err,
    });
  } else if (err.response && err.response.data) {
    if (
      err.response &&
      err.response.headers &&
      err.response.headers["content-type"] &&
      `${err.response.headers["content-type"]}`.startsWith("text/html")
    ) {
      const tempDiv = window.document.createElement("div");
      tempDiv.innerHTML = err.response.data;
      const message = tempDiv.innerText;
      tempDiv.remove();
      return new ErrorWrapper(ERRORS.API, message);
    } else {
      return new ErrorWrapper(ERRORS.API, err.response.data);
    }
  } else {
    return new ErrorWrapper(
      ERRORS.API,
      `Server connection failed: ${trimUrl(err.config.url)}`
    );
  }
}

function makeHttpError(err) {
  switch (err.httpStatus) {
    case 401:
      return new ErrorWrapper(
        ERRORS.AUTH,
        "Invalid username or password"
      );
    case 404:
      return new ErrorWrapper(ERRORS.API, "Not found", {
        httpStatus: err.httpStatus,
      });
    default:
      return new ErrorWrapper(ERRORS.GENERIC, `HTTP error ${err.httpStatus}`);
  }
}

export function toError(err) {
  if (err.isAxiosError) {
    return makeAxiosError(err);
  } else if (err.httpStatus) {
    return makeHttpError(err);
  } else {
    return new ErrorWrapper(ERRORS.GENERIC, err.message);
  }
}

export function prepareParams(query) {
  const data = {};
  if (query.page) {
    data.page = query.page;
  }
  if (query.sort) {
    data.sort = query.sort;
  }
  if (query.filter) {
    if (Array.isArray(query.filter) && query.filter.length === 1) {
      data.filter = query.filter[0];
    } else {
      data.filter = query.filter;
    }
  }
  if (query.filterJson) {
    data["filter-json"] = JSON.stringify(query.filterJson);
  }

  return data;
}

function prepareUrl(url) {
  if (!url || url.indexOf("http://") === 0 || url.indexOf("https://") === 0)
    return url;
  return `${apiUrl}${url}`;
}
/*
export async function auth(username, password) {
  return new Promise((resolve, reject) => {
    axios
      .post(
        prepareUrl("/auth/login"),
        {},
        {
          auth: {
            username,
            password,
          },
        }
      )
      .then((resp) => {
        store.commit("setToken", resp.headers.authorization);
        resolve(resp.data);
      })
      .catch((err) => reject(toError(err)));
  });
}
*/

export async function auth(username, password) {
  return new Promise((resolve, reject) => {
    const data = new URLSearchParams();
    data.append("username", username);
    data.append("password", password);

    axios({
      method: "POST",
      url: `${apiUrl}/auth/login`,
      data,
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
    })
      .then((resp) => resolve(resp.data))
      .catch((err) => reject(toError(err)));
  });
}

export async function get(url, params) {
  return new Promise((resolve, reject) => {
    const Authorization = store.getters.token;
    if (!Authorization) {
      reject(new ErrorWrapper(ERRORS.AUTH, "User not authenticated"));
    }
    const options = { headers: { Authorization } };
    if (params) {
      options.params = params;
      options.paramsSerializer = function (params) {
        return qs.stringify(params, { arrayFormat: "repeat" });
      };
    }
    axios
      .get(prepareUrl(url), options)
      .then((resp) => resolve(resp.data))
      .catch((err) => reject(toError(err)));
  });
}

export async function put(url, data) {
  return new Promise((resolve, reject) => {
    const Authorization = store.getters.token;
    if (!Authorization) {
      reject(new ErrorWrapper(ERRORS.AUTH, "User not authenticated"));
    }
    axios
      .put(prepareUrl(url), data, { headers: { Authorization } })
      .then((resp) => resolve(resp.data))
      .catch((err) => reject(toError(err)));
  });
}

export async function patch(url, data) {
  return new Promise((resolve, reject) => {
    const Authorization = store.getters.token;
    if (!Authorization) {
      reject(new ErrorWrapper(ERRORS.AUTH, "User not authenticated"));
    }
    axios
      .patch(prepareUrl(url), data, {
        headers: { Authorization },
      })
      .then((resp) => resolve(resp.data))
      .catch((err) => reject(toError(err)));
  });
}

export async function post(url, data, contentType) {
  return new Promise((resolve, reject) => {
    const Authorization = store.getters.token;
    if (!Authorization) {
      reject(new ErrorWrapper(ERRORS.AUTH, "User not authenticated"));
    }
    let options = {};
    if (contentType) {
      if (typeof contentType === "string")
        options.headers["Content-Type"] = contentType;
      else options = { ...options, ...contentType };
    }
    options.headers = { Authorization };
    axios
      .post(prepareUrl(url), data, options)
      .then((resp) => resolve(resp.data))
      .catch((err) => reject(toError(err)));
  });
}

export async function del(url) {
  return new Promise((resolve, reject) => {
    const Authorization = store.getters.token;
    if (!Authorization) {
      reject(new ErrorWrapper(ERRORS.AUTH, "User not authenticated"));
    }
    axios
      .delete(prepareUrl(url), { headers: { Authorization } })
      .then((resp) => resolve(resp.data))
      .catch((err) => reject(toError(err)));
  });
}
