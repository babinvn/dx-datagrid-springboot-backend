import {
  get,
  put,
  post,
  patch,
  del,
  auth,
  ERRORS,
  toError,
} from "./base";

function crud(name, endpoint) {
  const result = {};
  result[`find${name}s`] = (params) => get(endpoint, params);
  result[`find${name}`] = result[`find${name}s`];
  result[`get${name}s`] = result[`find${name}s`];
  result[`get${name}`] = (id) => get(`${endpoint}/${id}`);
  result[`post${name}`] = (data) => post(endpoint, data);
  result[`put${name}`] = (id, data) => put(`${endpoint}/${id}`, data);
  result[`patch${name}`] = (id, data) => patch(`${endpoint}/${id}`, data);
  result[`delete${name}`] = (id) => del(`${endpoint}/${id}`);
  result[`lookup${name}s`] = (params) => get(`${endpoint}/lookup`, params);
  return result;
}

const services = {
  auth,
  ERRORS,
  toError,
  ...crud("User", "/users"),
  ...crud("Role", "/roles"),
};

export { auth } from "./base";
export default services;
