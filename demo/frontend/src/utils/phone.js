export const formatPhoneNumber = (phoneNumber) =>
  phoneNumber
    ? phoneNumber.replace(
        /^(\+\d+)(\d{3})(\d{3})(\d{2})(\d{2})$/,
        (f, g1, g2, g3, g4, g5) => {
          return `${g1} (${g2}) ${g3}-${g4}-${g5}`;
        }
      )
    : phoneNumber;
