import Vue from "vue";
import Vuex from "vuex";
import createCache from "vuex-cache";
import API from "../services";

Vue.use(Vuex);

function parseStorage(key, def) {
  try {
    return JSON.parse(localStorage[key]);
  } catch {
    return def || null;
  }
}

export default new Vuex.Store({
  state: {
    apiUrl: process.env.VUE_APP_API_URL,
    token: localStorage[`${process.env.VUE_APP_API_CODE}-TOKEN`],
    user: parseStorage(`${process.env.VUE_APP_API_CODE}-USER`, {}),
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
      localStorage[`${process.env.VUE_APP_API_CODE}-TOKEN`] = token;
    },
    setUser(state, user) {
      state.user = user;
      localStorage[`${process.env.VUE_APP_API_CODE}-USER`] = JSON.stringify(
        user
      );
    },
  },
  actions: {
    getRoles: async () => (await API.getRoles()).content,
    getUsers: async () => (await API.getRoles()).content,
    getRefDataUserStates: () => [
      { code: 'ACTI', name: 'Active' },
      { code: 'DELD', name: 'Deleted' },
      { code: 'SUSP', name: 'Suspended' },
      { code: 'PEND', name: 'Pending' }
    ],
  },
  modules: {},
  getters: {
    apiUrl(state) {
      return state.apiUrl;
    },
    token(state) {
      return state.token;
    },
    user(state) {
      return state.user;
    },
  },
  plugins: [createCache()],
});
