CREATE SEQUENCE HIBERNATE_SEQUENCE INCREMENT 1 START 1000000;

CREATE SEQUENCE roles_seq INCREMENT 1 START 1;
CREATE TABLE roles (
    id NUMERIC(10) DEFAULT roles_seq.nextval PRIMARY KEY,
    code VARCHAR(10) NOT NULL,
    name VARCHAR(255),
    UNIQUE KEY (code)
);
COMMENT ON TABLE roles IS 'System roles';
COMMENT ON COLUMN roles.id IS 'Key';
COMMENT ON COLUMN roles.code IS 'Code';
COMMENT ON COLUMN roles.name IS 'Name';

INSERT INTO roles (code, name) VALUES ('ADMIN', 'System Administrator');
INSERT INTO roles (code, name) VALUES ('USER', 'Customer user');
INSERT INTO roles (code, name) VALUES ('TEST', 'Test user');



CREATE SEQUENCE users_seq INCREMENT 1 START 1;
CREATE TABLE users (
    id NUMERIC(10) DEFAULT users_seq.nextval PRIMARY KEY,
    username VARCHAR(40) NOT NULL,
    display_name VARCHAR(255) NOT NULL,
    email VARCHAR(255),
    state CHAR(4) DEFAULT 'PEND' NOT NULL,
    password_hash VARCHAR(255),
    phone_number VARCHAR(20),
    create_date TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    UNIQUE KEY (username),
    UNIQUE KEY (email),
    CHECK (state in ('ACTI','DELD','SUSP','PEND'))
);
COMMENT ON TABLE users IS 'Users';
COMMENT ON COLUMN users.id IS 'ID';
COMMENT ON COLUMN users.username IS 'User name';
COMMENT ON COLUMN users.display_name IS 'Display name';
COMMENT ON COLUMN users.email IS 'Email';
COMMENT ON COLUMN users.state IS 'User state';
COMMENT ON COLUMN users.password_hash IS 'Password';
COMMENT ON COLUMN users.phone_number IS 'Phone number';

INSERT INTO users (username, display_name, state, password_hash) VALUES (
  'admin', 'Administrator', 'ACTI', '$2a$10$ejR3uq.tspjVr0pTSb4NX.Zi4ZfKzivXYPIQRXAzPIWz/aFFivreq'
);



CREATE TABLE user_roles (
    role_id NUMERIC(10) NOT NULL,
    user_id NUMERIC(10) NOT NULL,
    PRIMARY KEY (role_id, user_id),
    FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE,
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);
COMMENT ON TABLE user_roles IS 'User roles';
COMMENT ON COLUMN user_roles.role_id IS 'Role';
COMMENT ON COLUMN user_roles.user_id IS 'User';

INSERT INTO user_roles (role_id, user_id) VALUES (1, 1);

COMMIT;