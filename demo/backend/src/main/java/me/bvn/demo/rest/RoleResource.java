package me.bvn.demo.rest;

import javax.ws.rs.Path;
import lombok.Getter;
import me.bvn.demo.domain.Role;
import me.bvn.demo.repo.RoleRepository;
import me.bvn.services.base.EntityResource;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/roles")
public class RoleResource extends EntityResource<Role, RoleRepository> {
    @Autowired
    @Getter
    private RoleRepository repository;
}
