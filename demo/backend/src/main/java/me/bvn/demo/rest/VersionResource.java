package me.bvn.demo.rest;

import java.io.Serializable;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

@Path("/version")
public class VersionResource {
    @Autowired
    private Environment env;

    @GET
    @Produces("application/json")
    public VersionInfo version(){
        return new VersionInfo(env.getProperty("app.version.number"));
    }

    @AllArgsConstructor
    public static class VersionInfo implements Serializable {
        @Getter
        private final String version;
    }
}