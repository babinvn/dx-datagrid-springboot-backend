package me.bvn.demo.repo;

import me.bvn.demo.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {
    User findByUsername(String username);
    User findByPhoneNumber(String phoneNumber);
    User findByEmail(String email);
    boolean existsByUsername(String username);

    @Modifying(flushAutomatically = true)
    @Query(value = "UPDATE users SET ts = CURRENT_TIMESTAMP WHERE username = :subject OR email = :subject", nativeQuery = true)
    void updateUserTimestamp(@Param("subject") String subject);
}
