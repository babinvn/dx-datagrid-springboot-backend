package me.bvn.demo.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;
import lombok.Getter;
import me.bvn.demo.domain.User;
import me.bvn.demo.repo.UserRepository;
import me.bvn.services.base.EntityResource;
import org.springframework.beans.factory.annotation.Autowired;

@Path("/users")
public class UserResource extends EntityResource<User, UserRepository> {
    @Autowired
    @Getter
    private UserRepository repository;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/me")
    public User me(@Context SecurityContext securityContext){
        return repository.findByUsername(securityContext.getUserPrincipal().getName());
    }
}
