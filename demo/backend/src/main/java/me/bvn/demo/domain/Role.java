package me.bvn.demo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Collection;
// import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import me.bvn.demo.aspect.AuditSetter;
import me.bvn.services.base.EntityBean;
// import org.hibernate.annotations.Cache;
// import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "roles")
@SuppressWarnings("PersistenceUnitPresent")
@EqualsAndHashCode(of="id", callSuper=false)
@ToString(exclude = "users")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "users"})
@XmlRootElement
// @Cacheable
// @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Role extends EntityBean<Role> {
    @Id
    // @GeneratedValue(strategy = GenerationType.IDENTITY) -- not working for H2
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long id;

    @NotNull
    @Column(name = "code", length = 10)
    @Getter
    @Setter(onMethod_={@AuditSetter})
    private String code;

    @NotNull
    @Column(name = "name", length = 255)
    @Getter
    @Setter(onMethod_={@AuditSetter})
    private String name;

    @Getter
    @Setter(onMethod_={@AuditSetter})
    @ManyToMany(mappedBy="roles")
    private Collection<User> users;
}