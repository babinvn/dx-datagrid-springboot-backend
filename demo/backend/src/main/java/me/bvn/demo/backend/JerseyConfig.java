package me.bvn.demo.backend;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Path;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.StandardServletEnvironment;

@Component
@ApplicationPath("/rest")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() throws ClassNotFoundException {
        // register(MyException.class);
        // register(AuthResource.class);
        registerClasses(getClasses("me.bvn.demo.rest"));
    }

    private Set<Class<?>> getClasses(String packageName) throws ClassNotFoundException {
        final Set<Class<?>> result = new HashSet<>();
        final ClassPathScanningCandidateComponentProvider provider =
            new ClassPathScanningCandidateComponentProvider(true, new StandardServletEnvironment());
        provider.addIncludeFilter(new AnnotationTypeFilter(Path.class));
        for (BeanDefinition beanDefinition : provider.findCandidateComponents(packageName)) {
            result.add(Class.forName(beanDefinition.getBeanClassName()));
	}
        return result;
    }
}
