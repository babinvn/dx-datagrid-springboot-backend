package me.bvn.demo.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.security.Key;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.Response;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import me.bvn.demo.backend.SpringContext;
import me.bvn.demo.domain.User;
import me.bvn.demo.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;

@Path("/auth")
public class AuthResource {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private Environment env;
    @Autowired
    private ObjectMapper objectMapper;
    
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response passwordLogin(@FormParam("username") String username, @FormParam("password") String password) {
        User user = userRepository.findByUsername(username);
        if (user == null) user = userRepository.findByEmail(username);
        if (user == null || !passwordEncoder.matches(password, user.getPasswordHash()))
            return Response.status(Response.Status.UNAUTHORIZED).entity("Invalid username or password").build();

        ObjectNode result = objectMapper.createObjectNode();
        result.put("token", getJWTToken(user));
        result.putPOJO("user", user);

        return Response.status(Response.Status.ACCEPTED).entity(result).build();
    }

    private String getJWTToken(User user) {
        return AuthResource.getJWTToken(env, user);
    }
    
    public static String getJWTToken(Environment env, User user) {
        String jwtId = env.getProperty("app.security.jwt.id");
        String jwtSecret = env.getProperty("app.security.jwt.secret");

        List<GrantedAuthority> authorities = user
            .getRoles()
            .stream()
            .map(role -> new SimpleGrantedAuthority(role.getCode()))
            .collect(Collectors.toList());

        if (user.getId() != null) {
            authorities.add(new SimpleGrantedAuthority(String.format("USER_%d", user.getId())));
        }

        Key key = Keys.hmacShaKeyFor(jwtSecret.getBytes());
        String token = Jwts
            .builder()
            .setId(jwtId)
            .setSubject(user.getUsername())
            .claim("authorities", authorities)
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + 8 * 60 * 60 * 1000L)) // 8 hours
            .signWith(key, SignatureAlgorithm.HS512)
            .compact();

        return "Bearer " + token;
    }

    public AuthResource() {
        Environment env = SpringContext.getBean(Environment.class);
    }
}