package me.bvn.demo.backend;

import java.util.Arrays;
import java.util.Map;
import javax.ws.rs.HttpMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;
import org.springframework.web.context.request.WebRequest;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final String[] HTTP_METHODS = {"HEAD", "DELETE", "POST", "GET", "OPTIONS", "PATCH", "PUT", "MERGE"};

    private final Environment env;

    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    @Bean
    public HttpFirewall getHttpFirewall() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowedHttpMethods(Arrays.asList(HTTP_METHODS));
        return firewall;
    }
    @Bean
    public ErrorAttributes errorAttributes() {
        return new DefaultErrorAttributes() {
            @Override
            public Map<String, Object> getErrorAttributes(WebRequest webRequest, ErrorAttributeOptions options) {
                Map<String, Object> errorAttributes = super.getErrorAttributes(webRequest, options);
                if (!errorAttributes.containsKey("message") || errorAttributes.get("message").toString().isEmpty()) {
                    errorAttributes.put("message", errorAttributes.get("error"));
                }
                if (errorAttributes.get("error") != null && errorAttributes.get("error") instanceof String) {
                    errorAttributes.remove("error");
                }
                return errorAttributes;
            }
        };
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(userDetailsService)
            .passwordEncoder(new BCryptPasswordEncoder());
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .addFilterAfter(new JWTAuthorizationFilter(env), UsernamePasswordAuthenticationFilter.class)
            .authorizeRequests()
                .antMatchers("/rest/version").permitAll()
                .antMatchers("/rest/auth/login").permitAll()
                .antMatchers("/rest/**").hasAuthority("ADMIN")
                .antMatchers(HttpMethod.GET, "/rest/**").authenticated()

                .antMatchers("/").permitAll()
                .antMatchers("/error").permitAll()
                .antMatchers("/index.html").permitAll()
                .antMatchers("/license.html").permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/fonts/**").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("/js/**").permitAll()

                .anyRequest().denyAll()
            .and()
                .headers().frameOptions().disable();
    }

    public SecurityConfig(Environment env) {
        this.env = env;
    }
}
