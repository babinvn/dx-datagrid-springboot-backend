package me.bvn.demo.aspect;

import me.bvn.services.base.EntityBean;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class AuditSetterAspect {
    @After("execution(* me.bvn.demo.domain.*.*(..)) && @annotation(AuditSetter)")
    public void after(JoinPoint joinPoint) throws Throwable {
        if (joinPoint.getTarget() instanceof EntityBean) {
            EntityBean eb = (EntityBean)joinPoint.getTarget();
            eb.addModifiedField(joinPoint.getSignature().getName());
        }
    }
}
