package me.bvn.demo.backend;

import java.util.List;
import java.util.stream.Collectors;
import me.bvn.demo.domain.User;
import me.bvn.demo.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserDetailsService {
    private static final String USER_ACTIVE = "ACTI";

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null)
            user = userRepository.findByEmail(username);
        if (user == null)
            throw new UsernameNotFoundException(username);

        List<GrantedAuthority> authorities = user
            .getRoles()
            .stream()
            .map(role -> new SimpleGrantedAuthority(role.getCode()))
            .collect(Collectors.toList());
        authorities.add(new SimpleGrantedAuthority(String.format("USER_%d", user.getId())));

        return org.springframework.security.core.userdetails.User
            .withUsername(user.getUsername())
            .password(user.getPasswordHash())
            .accountLocked(!USER_ACTIVE.equals(user.getState()))
            .authorities(authorities)
            .build();
    }
}