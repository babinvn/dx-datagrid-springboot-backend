package me.bvn.demo.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.sql.Timestamp;
import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import me.bvn.demo.aspect.AuditSetter;
import me.bvn.services.base.EntityBean;

@Entity
@Table(name = "users")
@SuppressWarnings({"PersistenceUnitPresent", "ValidAttributes"})
@EqualsAndHashCode(of="id", callSuper=false)
@ToString(exclude = "roles")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "passwordHash"})
@XmlRootElement
// @Cacheable
// @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class User extends EntityBean<User> {
    public static final String STATE_PENDING = "PEND";
    public static final String STATE_ACTIVE = "ACTI";

    @Id
    // @GeneratedValue(strategy = GenerationType.IDENTITY)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private Long id;

    @NotNull
    @Column(name = "username", length = 40)
    @Getter
    @Setter(onMethod_={@AuditSetter})
    private String username;

    @NotNull
    @Column(name = "display_name", length = 255)
    @Getter
    @Setter(onMethod_={@AuditSetter})
    private String displayName;

    @Column(name = "email", length = 255)
    @Getter
    @Setter(onMethod_={@AuditSetter})
    private String email;

    @Column(name = "phone_number", length = 20)
    @Getter
    @Setter(onMethod_={@AuditSetter})
    private String phoneNumber;

    @Column(name = "state", length = 4)
    @Getter
    @Setter(onMethod_={@AuditSetter})
    private String state;

    @Column(name = "password_hash")
    @Getter
    @Setter(onMethod_={@AuditSetter})
    private String passwordHash;

    @Getter
    @Setter(onMethod_={@AuditSetter})
    @ManyToMany(fetch=FetchType.LAZY)
    @JoinTable(name = "user_roles",
        joinColumns = @JoinColumn(name = "user_id"),
        inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Collection<Role> roles;

    @Column(name="create_date", updatable=false, insertable=false)
    @Getter
    private Timestamp createDate;
}
