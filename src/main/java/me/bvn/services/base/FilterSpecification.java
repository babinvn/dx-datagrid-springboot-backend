package me.bvn.services.base;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.persistence.metamodel.PluralAttribute;
import org.hibernate.query.criteria.internal.path.PluralAttributePath;
import org.springframework.data.jpa.domain.Specification;

public class FilterSpecification<E extends EntityBean<E>> implements Specification<E> {
    private final ArrayNode arr;
    private final Specification<E> securityPredicate;

    @Override
    public Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        if (securityPredicate == null)
            return toPredicate(root, query, builder, arr);
        else if (arr == null)
            return securityPredicate.toPredicate(root, query, builder);
        else
            return builder.and(securityPredicate.toPredicate(root, query, builder), toPredicate(root, query, builder, arr));
    }

    private Predicate toPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder builder, JsonNode node) {
        if (node.getNodeType() != JsonNodeType.ARRAY)
            throw new IllegalArgumentException("Expected JSON array node");
        ArrayNode arr = (ArrayNode)node;

        switch (arr.size()){
            case 0:
            case 1:
                throw new IllegalArgumentException(String.format("ArrayNode size of [%1$s] not supported", arr.size()));
            case 2:
                String notOperator = arr.get(0).asText();
                if (!notOperator.equals("!"))
                    throw new IllegalArgumentException("Filter array of size [2] have to be unary NOT operation");
                return builder.not(toPredicate(root, query, builder, arr.get(1)));
            case 3:
                switch (arr.get(0).getNodeType()){
                    case STRING:
                        return buildBinaryPredicate(root, query, builder, arr);
                    case ARRAY:
                        return buildGroupPredicate(root, query, builder, arr);
                    default:
                        throw new IllegalArgumentException(String.format("The type [%1$s] of the first element of filter array not supported", arr.get(0).getNodeType()));
                }
            default:
                if((arr.size() & 1) == 0)
                    throw new IllegalArgumentException(String.format("ArrayNode size of [%1$s] not supported", arr.size()));
                return buildGroupPredicate(root, query, builder, arr);
        }
    }
    private Predicate buildGroupPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder builder, ArrayNode arr) {
        List<Predicate> groupPredicates = new ArrayList<>();
        String groupOperator = arr.get(1).asText();        
        groupPredicates.add(toPredicate(root, query, builder, arr.get(0)));

        for (int i = 1; i < arr.size(); i += 2) {
            if (!groupOperator.equals(arr.get(0 + i).asText()))
                throw new IllegalArgumentException("Can not combine different group operators (OR, AND) on the same array level");
            groupPredicates.add(toPredicate(root, query, builder, arr.get(1 + i)));
        }

        switch (groupOperator){
            case "and":
                return builder.and(groupPredicates.toArray(new Predicate[groupPredicates.size()]));
            case "or":
                return builder.or(groupPredicates.toArray(new Predicate[groupPredicates.size()]));
            default:
                throw new IllegalArgumentException(String.format("Group operator [%1$s] not supported", groupOperator));
        }
    }
    private Predicate buildManyToManyPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder builder, ArrayNode arr, String[] propertyName) {
        String comparisonOperator = arr.get(1).asText();
        Path<Comparable> property = root.get(propertyName[0]);

        if (property instanceof PluralAttributePath) {
            PluralAttributePath path = (PluralAttributePath)property;
            PluralAttribute attr = path.getAttribute();
            Class bindableJavaType = attr.getBindableJavaType();

            String matchingAttribute = findMatchingAttributeWithMatchingPropertyNameAnnotation(root.getJavaType(), propertyName[0]);
            if (matchingAttribute == null)
                matchingAttribute = findMatchingAttribute(bindableJavaType, propertyName[0]);
            if (matchingAttribute == null) {
                throw new RuntimeException(
                    String.format("Failed to find an attribute in class %s matching %s.%s",
                        bindableJavaType.getName(),
                        root.getJavaType().getName(),
                        propertyName[0]
                    )
                );
            }

            query.distinct(true);
            // Create a subquery to select only those records that
            // match our filter expression
            Subquery subquery = query.subquery(bindableJavaType);
            Root bindableTypeRoot = subquery.from(bindableJavaType);
            Expression<Collection<E>> list = bindableTypeRoot.get(matchingAttribute);
            subquery.select(bindableTypeRoot);
            String filterPropertyName = propertyName.length > 1? propertyName[1]: "id";
            Path<Comparable> filterProperty = bindableTypeRoot.get(filterPropertyName);
            switch (comparisonOperator) {
                case "=":
                    subquery.where(
                        filterProperty.in(Arrays.asList( getValue(arr.get(2)) )),
                        builder.isMember(root, list)
                    );
                    break;
                case "<>":
                    subquery.where(
                        filterProperty.in(Arrays.asList( getValue(arr.get(2)) )).not(),
                        builder.isMember(root, list)
                    );
                    break;
                case "anyof":
                    subquery.where(
                        filterProperty.in( getCollection((ArrayNode)arr.get(2)) ),
                        builder.isMember(root, list)
                    );
                    break;
                case "noneof":
                    subquery.where(
                        filterProperty.in( getCollection((ArrayNode)arr.get(2)) ).not(),
                        builder.isMember(root, list)
                    );
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Unexpected operator for many-to-many relation: %s", comparisonOperator));
            }
            return builder.exists(subquery);
        } else {
            throw new RuntimeException(
                String.format("Property %s.%s should be of type PluralAttributePath, actual type is %s",
                    root.getJavaType().getName(),
                    propertyName[0],
                    property.getClass().getName()
                )
            );
        }
    }
    private Predicate buildOneToManyPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder builder, ArrayNode arr, String[] propertyName) {
        String comparisonOperator = arr.get(1).asText();
        Path<Comparable> property = root.get(propertyName[0]);

        if (property instanceof PluralAttributePath) {
            PluralAttributePath path = (PluralAttributePath)property;
            PluralAttribute attr = path.getAttribute();
            Class bindableJavaType = attr.getBindableJavaType();

            String matchingAttribute = findMatchingAttributeWithMatchingPropertyNameAnnotation(root.getJavaType(), propertyName[0]);
            if (matchingAttribute == null) {
                throw new RuntimeException(
                    String.format("Failed to find an attribute in class %s matching %s.%s",
                        bindableJavaType.getName(),
                        root.getJavaType().getName(),
                        propertyName[0]
                    )
                );
            }

            query.distinct(true);
            // Create a subquery to select only those records that
            // match our filter expression
            Subquery subquery = query.subquery(bindableJavaType);
            Root bindableTypeRoot = subquery.from(bindableJavaType);
            Expression<E> e = bindableTypeRoot.get(matchingAttribute);
            subquery.select(bindableTypeRoot);
            String filterPropertyName = propertyName.length > 1? propertyName[1]: "id";
            Path<Comparable> filterProperty = bindableTypeRoot.get(filterPropertyName);
            
            Predicate predicate = buildSimplePredicate(bindableTypeRoot, builder, filterPropertyName, filterProperty, comparisonOperator, arr.get(2));
            subquery.where(predicate, builder.equal(root, e));
            return builder.exists(subquery);
        } else {
            throw new RuntimeException(
                String.format("Property %s.%s should be of type PluralAttributePath, actual type is %s",
                    root.getJavaType().getName(),
                    propertyName[0],
                    property.getClass().getName()
                )
            );
        }
    }
    private Predicate buildSimplePredicate(Root<E> root, CriteriaBuilder builder, String propertyName, Path<Comparable> property, String comparisonOperator, JsonNode jsonValue) {
        if (jsonValue == null || jsonValue.isNull()) {
            switch (comparisonOperator){
                case "=":
                    return builder.isNull(property);
                case "<>":
                    return builder.isNotNull(property);
                default:
                    throw new IllegalArgumentException("Operand value null not supported with operator " + comparisonOperator);
            }
        }

        switch (comparisonOperator){
            case "anyof":
                return property.in(getCollection((ArrayNode)jsonValue));
            case "noneof":
                return property.in(getCollection((ArrayNode)jsonValue)).not();
            case "between":
                return builder.between(
                    property,
                    getValue(jsonValue.get(0), property.getJavaType()),
                    getValue(jsonValue.get(1), property.getJavaType())
                );
        }

        Comparable value = getValue(jsonValue, property.getJavaType());
        switch (comparisonOperator){
            case "=":
                return builder.equal(property, value);
            case "<>":
                return builder.notEqual(property, value);
            case ">":
                return builder.greaterThan(property, value);
            case ">=":
                return builder.greaterThanOrEqualTo(property, value);
            case "<":
                return builder.lessThan(property, value);
            case "<=":
                return builder.lessThanOrEqualTo(property, value);
            case "startswith":
                return builder.like(builder.upper(root.<String>get(propertyName)), String.format("%s%%", value.toString().toUpperCase()));
            case "endswith":
                return builder.like(builder.upper(root.<String>get(propertyName)), String.format("%%%s", value.toString().toUpperCase()));
            case "contains":
                return builder.like(builder.upper(root.<String>get(propertyName)), String.format("%%%s%%", value.toString().toUpperCase()));
            case "notcontains":
                return builder.like(builder.upper(root.<String>get(propertyName)), String.format("%%%s%%", value.toString().toUpperCase())).not();
            default:
                throw new IllegalArgumentException(String.format("Comparison operator [%1$s] not supported", comparisonOperator));
        }
    }

    private Predicate buildBinaryPredicate(Root<E> root, CriteriaQuery<?> query, CriteriaBuilder builder, ArrayNode arr) {
        // Arr: [property, operator, value]
        // Property name can be complex, example: "roles.code" (see examples below)
        String[] propertyName = arr.get(0).asText().split("\\.");
        String comparisonOperator = arr.get(1).asText();
        
        // Handle many-to-many realtionship. Examples:
        //  curl -G -H "Authorization: $TOKEN" --data-urlencode 'filter-json=["roles", "anyof", [1]]' http://localhost:8080/admin/rest/users
        //  curl -G -H "Authorization: $TOKEN" --data-urlencode 'filter-json=["roles.id", "anyof", [1]]' http://localhost:8080/admin/rest/users
        //  curl -G -H "Authorization: $TOKEN" --data-urlencode 'filter-json=["roles.code", "anyof", ["ADMIN"]]' http://localhost:8080/admin/rest/users
        //  curl -G -H "Authorization: $TOKEN" --data-urlencode 'filter-json=["roles.code", "noneof", ["ADMIN"]]' http://localhost:8080/admin/rest/users
        //  curl -G -H "Authorization: $TOKEN" --data-urlencode 'filter-json=["roles.code", "=", "ADMIN"]' http://localhost:8080/admin/rest/users
        //  curl -G -H "Authorization: $TOKEN" --data-urlencode 'filter-json=["roles.code", "<>", "ADMIN"]' http://localhost:8080/admin/rest/users

        if (isManyToManyRelation(root.getJavaType(), propertyName[0])) {
            return buildManyToManyPredicate(root, query, builder, arr, propertyName);
        } else if (isOneToManyRelation(root.getJavaType(), propertyName[0])) {
            return buildOneToManyPredicate(root, query, builder, arr, propertyName);
        }
        
        Path<Comparable> property = null;
        for (String propertyNamePart: propertyName) {
            if (property == null) {
                property = root.get(propertyNamePart);
            } else {
                property = property.get(propertyNamePart);
            }
        }
        return buildSimplePredicate(root, builder, propertyName[0], property, comparisonOperator, arr.get(2));
    }
    private Comparable getValue(JsonNode value) {
        if (value.isBigInteger())
            return value.bigIntegerValue();
        else if (value.isBigDecimal())
            return value.decimalValue();
        else if (value.isBoolean())
            return value.booleanValue();
        else if (value.isDouble())
            return value.asDouble();
        else if (value.isFloat())
            return value.floatValue();
        else if (value.isLong())
            return value.longValue();
        else if (value.isInt())
            return value.intValue();
        else if (value.isShort())
            return value.shortValue();
        else if (value.isTextual())
            return value.textValue();

        else if (value.isNumber())
            throw new IllegalArgumentException("Operand value should a comparable number");
        else if (value.isBinary())
            throw new IllegalArgumentException("Operand value should not be binary");
        else if (value.isArray())
            throw new IllegalArgumentException("Operand value should not be an array");
        else if (!value.isValueNode())
            throw new IllegalArgumentException("Operand value should be a value node");
        else if (value.isEmpty())
            throw new IllegalArgumentException("Operand value should not be empty");
        else if (value.isMissingNode())
            throw new IllegalArgumentException("Operand value should not be missing");
        else if (value.isNull())
            throw new IllegalArgumentException("Operand value should not be null");
        else if (value.isObject() || value.isPojo())
            throw new IllegalArgumentException("Operand value should not be a complex value");
        else
            throw new IllegalArgumentException("Unexpected operand type: " + value.getNodeType());
    }
    private Comparable getValue(JsonNode value, Class type) {
        if (type.equals(Timestamp.class)) {
            if (value.isNumber()) {
                return new Timestamp(value.asLong());
            } else if (value.isTextual()) {
                Instant i = Instant.parse(value.asText());
                return new Timestamp(i.toEpochMilli());
            }
        }
        return getValue(value);
    }
    private Collection getCollection(ArrayNode arr) {
        return StreamSupport
            .stream(arr.spliterator(), false)
            .map(node -> getValue(node))
            .collect(Collectors.toList());
    }

    public FilterSpecification(String json) {
        this(parseJson(json), null);
    }
    public FilterSpecification(ArrayNode arr) {
        this(arr, null);
    }
    public FilterSpecification(Specification<E> securityPredicate) {
        this.arr = null;
        this.securityPredicate = securityPredicate;
    }
    public FilterSpecification(String json, Specification<E> securityPredicate) {
        this(parseJson(json), securityPredicate);
    }
    public FilterSpecification(ArrayNode arr, Specification<E> securityPredicate) {
        this.arr = arr;
        this.securityPredicate = securityPredicate;
    }
    
    
    public static ArrayNode parseJson(String json) {
        if (json == null)
            return null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode arr = mapper.readTree(json);
            return (ArrayNode)arr;
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(ex);
        }
    }
    private static String findMatchingAttributeWithMatchingPropertyNameAnnotation(Class type, String property) {
        try {
            Field field = type.getDeclaredField(property);
            MatchingPropertyName name = field.getAnnotation(MatchingPropertyName.class);
            if (name != null)
                return name.value();
            OneToMany om = field.getAnnotation(OneToMany.class);
            return om == null? null: om.mappedBy();
        } catch (NoSuchFieldException | SecurityException ex) {
            return null;
        }
    }
    private static String findMatchingAttribute(Class type, String mappedBy) {
        for (Field field : type.getDeclaredFields()) {
            ManyToMany mm = field.getAnnotation(ManyToMany.class);
            if (mm != null && mm.mappedBy() != null && mm.mappedBy().equals(mappedBy)) {
                return field.getName();
            }
        }
        return null;
    }
    private boolean isManyToManyRelation(Class type, String fieldName) {
        try {
            Field field = type.getDeclaredField(fieldName);
            return field.getAnnotation(ManyToMany.class) != null;
        } catch (NoSuchFieldException | SecurityException ex) {}
        return false;
    }
    private boolean isOneToManyRelation(Class type, String fieldName) {
        try {
            Field field = type.getDeclaredField(fieldName);
            return field.getAnnotation(OneToMany.class) != null;
        } catch (NoSuchFieldException | SecurityException ex) {}
        return false;
    }
}
