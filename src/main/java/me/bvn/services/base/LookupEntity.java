package me.bvn.services.base;

import java.io.Serializable;

public abstract class LookupEntity<E extends LookupEntity> implements Serializable {
    public abstract Long getId();
}
