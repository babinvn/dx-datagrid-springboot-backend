package me.bvn.services.base;

import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Transactional;

public abstract class EntityResource<E extends EntityBean<E>, R extends JpaRepository<E, Long> & JpaSpecificationExecutor<E>> {
    public static final int DEFAUTL_PAGE = 100;

    public abstract R getRepository();

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public E get(@PathParam("id") Long id) {
        return getRepository().getOne(id);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Page<E> find(
        @QueryParam("skip") String skip,
        @QueryParam("take") String take,
        @QueryParam("sort") List<String> sort,
        @QueryParam("filter-json") String filterJson
    ) {
        if (filterJson == null)
            return getRepository().findAll(createPageable(skip, take, sort));
        else
            return getRepository().findAll(new FilterSpecification(filterJson), createPageable(skip, take, sort));
    }

    @Transactional
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public E post(E data) {
        return getRepository().saveAndFlush(data);
    }

    @Transactional
    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public E put(@PathParam("id") Long id, E data) {
        if (id == null || !id.equals(data.getId()))
            throw new IllegalArgumentException("Id does not match the data object");
        return getRepository().saveAndFlush(data);
    }

    @Transactional
    @PATCH
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public E patch(@PathParam("id") Long id, E data) {
        E entity = getRepository().getOne(id);
        data.applyTo(entity);
        return getRepository().saveAndFlush(entity);
    }

    @Transactional
    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") Long id) {
        getRepository().deleteById(id);
    }

    public static Pageable createPageable(String skip, String take, List<String> sort) {
        int _skip = skip == null? 0: Integer.parseInt(skip);
        int _take = take == null? DEFAUTL_PAGE: Integer.parseInt(take);

        if (sort == null)
            return PageRequest.of(_skip / _take, _take);

        List<Sort.Order> order = sort.stream().map(ss -> {
            String[] s = ss.split(",");
            if (s.length == 1)
                return Sort.Order.by(s[0]);
            else if ("DESC".equalsIgnoreCase(s[1]))
                return Sort.Order.desc(s[0]);
            else
                return Sort.Order.asc(s[0]);
        }).collect(Collectors.toList());
        return PageRequest.of(_skip / _take, _take, Sort.by(order));
    }
}
