package me.bvn.services.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public abstract class EntityBean<E extends EntityBean> implements Serializable {
    @JsonIgnore
    private final List<String> _modifiedFields = new ArrayList<>();
    public abstract Long getId();
    public void addModifiedField(String setterName) {
        _modifiedFields.add(setterName);
    }
    public void applyTo(E target) {
        try {
            for (String setterName: _modifiedFields) {
                Method setter = findMethod(target, setterName);
                Method getter = findMethod(this, "get" + setterName.substring(3));
                Object value = getter.invoke(this);
                setter.invoke(target, value);
            }
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new RuntimeException(ex);
        }
    }

    private Method findMethod(Object obj, String methodName) {
        for (Method m : obj.getClass().getDeclaredMethods()) {
            if (m.getName().equals(methodName)) {
                return m;
            }
        }
        throw new RuntimeException(String.format("Failed to find setter %s in %s", methodName, obj.getClass().getName()));
    }
}
