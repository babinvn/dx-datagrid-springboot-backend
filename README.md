# README

There is an awesome control [DevExtreme DataGrid](https://js.devexpress.com/Documentation/ApiReference/UI_Components/dxDataGrid/).
You can use it as a base for your database-powered web applications. This library provides some basic SpringBoot classes and a [demo
app](demo) with SpringBoot backend and Vuejs frontend.

To build the library:
```
gradle build
```
In order for the demo to compile you need to publish the library to your local maven repository:
```
gradle publishToMavenLocal
```
or use oneliner:
```
gradle build publishToMavenLocal
```

## Demo App

Demo app is based on default boilerplate provided by [DevExtreme CLI](https://js.devexpress.com/Documentation/Guide/Common/DevExtreme_CLI/).
Please check this [article](https://js.devexpress.com/Documentation/Guide/Vue_Components/Create_a_DevExtreme_Application/) for details.

The app adds two pages to the boilerplate: Roles and Users, based on their respective database [tables](demo/backend/src/main/resources/data.sql).

![Screenshot](Screenshot.png)

To build backend:
```
cd demo/backend
gradle build
```
To run backend standalone for debugging:
```
java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 \
-jar build/libs/backend-0.0.1.jar \
--spring.profiles.active=dev
```

To run frontend with hot-reload enabled:
```
cd demo/frontend
yarn install
yarn serve
```
This way you can debug and develop frontend.

Building backend for "production" will package frontend into SpringBoot application:
```
cd demo/backend
gradle -Pprod build
```
Run backend and now you can access the app at http://localhost:8080/demo/

Finally to build and run with docker:
```
docker build -t demo/dx-datagrid .

docker run --rm --name demo_dx_datagrid -d \
-p 8180:8080 \
-p 5180:5005 \
demo/dx-datagrid
```
This way you can access the app at http://docker:8180/demo/ (or whatever your docker machine host address).


## Testing

Get app version:
```
curl http://localhost:8080/demo/rest/version
```
Authenticate:
```
export USERNAME=admin
export PASSWORD=pifagor
curl -s -H "Content-Type: application/x-www-form-urlencoded" \
-d username=$USERNAME -d password=$PASSWORD \
-X POST "http://localhost:8080/demo/rest/auth/login"
```
or to create and export a token:
```
export TOKEN=$(curl -s -H "Content-Type: application/x-www-form-urlencoded" \
-d username=$USERNAME -d password=$PASSWORD \
-X POST "http://localhost:8080/demo/rest/auth/login" \
| jq -r .token)
```
Get current user:
```
curl -v -H "Authorization: $TOKEN" "http://localhost:8080/demo/rest/users/me"
```
List users:
```
curl -v -H "Authorization: $TOKEN" "http://localhost:8080/demo/rest/users"
```
etc
